//dependencies
	require('dotenv').config();
	const express = require('express');
	const connectDB = require('./config/db')
	const productRoutes = require('./routes/productRoutes');
	const userRoutes = require('./routes/userRoutes');
	const orderRoutes = require('./routes/orderRoutes')
	const cors = require("cors");

//mongoose connection
	connectDB();

//middlewares
	const app = express();
	app.use(express.json());
	app.use(cors());

//routers
	app.use('/products', productRoutes)
	app.use('/user', userRoutes);
	app.use('/order', orderRoutes);

//port listener
	const PORT = process.env.PORT || 5000;
	app.listen(PORT, ()	=> console.log(`Server running on ${PORT}`));