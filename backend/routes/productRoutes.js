const express = require('express');
const router = express.Router();
const auth = require('../auth');

const {
	getAllProducts,
	getProductById,
	createProduct,
	updateProduct,
	archiveProduct
} = require('../controller/productControllers');

router.get('/', getAllProducts);
router.get('/:id', getProductById);
router.post('/create-new', auth.verify, createProduct);
router.put('/update/:productId', auth.verify, updateProduct);
router.put('/:productId/archive', auth.verify, archiveProduct);

module.exports = router;