const User = require('../models/User');
const Product = require('../models/Product');
const Order = require('../models/Order')
const bcrypt = require('bcrypt');
const auth = require('../auth');


//order item
	const orderProduct = async (req, res, data) => {

		let checkUser = await User.findById(data.userId).then(user => {
			let data = {
			payload : auth.decode(req.headers.authorization),
			userId: req.body.userId,
			productId: req.body.productId
		};
		let {buyer} =  data.userId;
			if(data.payload.isAdmin === false) {
				let addOrder =  Product.findById(data.productId).then(product => {
					const newOrder = new Order({
						amount: product.price,
						address: req.body.address
					});
				newOrder.products.push({productId:data.productId});
				newOrder.buyer.push({userId: data.userId});
					return newOrder.save().then((total, err) => {
						const checkout = (err) ? false : total;
						res.json(checkout);
					})
				})
			} else {
				res.json(`An error occured`);
			}
		})
	};

//get all orders (ADMIN)
	const checkAllOrders = async (req, res) => {
		const data = auth.decode(req.headers.authorization);
		if(data.isAdmin){
			return Order.find({}).then(result => {
				res.json(result);
			})
		} else {
			res.json(`Administrative authorization required`);
		}
	};

//get orders (USER)
	const getBuyerOrders = async (req, res) => {
		const data = auth.decode(req.headers.authorization);
		return Order.find({buyer:{userId : data.id}}).then(result => {
			if(result === []){
				res.json(`No orders found`);
			} else {
				res.json(result);
			}
		})
	};

//exports
	module.exports = {
		orderProduct,
		checkAllOrders,
		getBuyerOrders
	};