const User = require('../models/User');
const Product = require('../models/Product');
const bcrypt = require('bcrypt');
const auth = require('../auth');

//register user
	const registerUser = async (req, res) => {
		return User.find({email: req.body.email}).then(result => {
			if (result.length > 0) {
				res.json(`${req.body.email} already exists`);
			} 
			else {
				let newUser = new User({
					username: req.body.username,
					email: req.body.email,
					password: bcrypt.hashSync(req.body.password,10)
				});

				return newUser.save().then((user, err) => {
					const savedUser = (err) ? false : user;
					res.json(savedUser);
				})
			}
		})
	};

//login user
	const loginUser = async (req, res) => {
		return User.findOne({email: req.body.email}).then(result => {
			if(result === null) {
				res.json(`${req.body.email} does not exist`);
			} else {
				const checkPassword = bcrypt.compareSync(req.body.password, result.password);
				if(checkPassword) {
					res.json({access: auth.createAccessToken(result)});
				} else {
					res.json(`Incorrect password`);
				}
			}
		})
	};

//set as admin
	const setAsAdmin = async (req, res) => {
		const data = {
			userId: req.params.userId,
			payload: auth.decode(req.headers.authorization),
		}
		return await User.findById(data.userId).then((result, err) => {
			if(data.payload.isAdmin) {
				result.isAdmin = true;
				return result.save().then((updatedUser, err) => {
					const newAdmin = (err) ? false : updatedUser;
					res.json(newAdmin);
				})
			} else {
				res.json(`Administrative permission required`)
			}
		})
	};

//get all user
	const getAllUser = async (req, res) => {
		payload = auth.decode(req.headers.authorization);
		if(payload.isAdmin){
			return await User.find({}).then((result, err) => {
			res.json(result)
			})
		} else {
			res.json(`Administrative authorization required`)
		}
	}	

//exports
	module.exports = {
		registerUser,
		loginUser,
		setAsAdmin,
		getAllUser
	};