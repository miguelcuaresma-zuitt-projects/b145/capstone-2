import { Link } from "react-router-dom";
import "./Navbar.css";
import { useSelector } from "react-redux";

const Navbar = ({click}) => {

  const cart = useSelector(state => state.cart);
  const {cartItems} = cart;

  const getCartCount = () => {
    return cartItems.reduce((qty, item) => qty + Number(item.qty), 0)
  };
  return (
    <nav className="navbar">
      <div className="navbar-logo">
        <h2>Shopping Cart</h2>
      </div>
      <ul className="navbar-links">
        <li>
          <Link to="/cart" className="cart_link">
            <i className="fas fa-shopping-cart"></i>
            <span>
              <span className="cartlogo_badge">{getCartCount()}</span>
            </span>
            <span>Cart</span>
          </Link>
        </li>
        <li>
          <Link to="/">Shop</Link>
        </li>
      </ul>
      <div className="hamburger_menu" onClick={click}>
      <div></div>
      <div></div>
      <div></div>
      </div>

    </nav>
  );
};

export default Navbar;
