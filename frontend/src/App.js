  import "./App.css";
  import { BrowserRouter as Router, Routes, Route } from "react-router-dom";
  import {useState} from "react"

// Screens
  import HomeScreen from "./screens/HomeScreen.js";
  import ProductScreen from "./screens/ProductScreen";
  import CartScreen from "./screens/CartScreen";
  import React from "react";

//Components
  import Navbar from "./components/Navbar"
  import Backdrop from "./components/Backdrop"
  import SideDrawer from "./components/SideDrawer"

function App() {
  const [sideToggle, setSideToggle] = useState(false)
  return (
    <Router>
      <Navbar click={() => setSideToggle(true)}/>
      <Backdrop show={sideToggle} click={() => setSideToggle(false)} />
      <SideDrawer show={sideToggle} />
      <main>
        <Routes>
          <Route path="/" element={<HomeScreen/>} />
          <Route path="/products/:id" element={<ProductScreen/>} />
          <Route path="/cart" element={<CartScreen/>} />
        </Routes>
      </main>
    </Router>
  );
}

export default (App);
