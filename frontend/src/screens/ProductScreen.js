import './ProductScreen.css';
import { useState, useEffect } from "react";
import { useSelector, useDispatch } from "react-redux";
import { useParams, useNavigate } from "react-router-dom";


//actions
    import { getProductDetails } from "../redux/actions/productActions";
    import { addToCart } from "../redux/actions/cartActions";
    
const ProductScreen = ({match}) => {

    const { id } = useParams();
    
    
    const [qty, setQty] = useState(1);
    const dispatch = useDispatch();

    const productDetails = useSelector((state) => state.getProductDetails);
    let { loading, error, product } = productDetails;

    useEffect(() => {
        
        if (product && id !== product._id) {
          dispatch(getProductDetails(id));
        }
      }, [dispatch, match, product, id]);
      const navigate = useNavigate();

      const addToCartHandler = () => {
        dispatch(addToCart(product._id, qty));
        navigate(`/cart`);
      };

    return (
        <div className="productscreen">
            {loading ? <h2>Loading...</h2> : error ? <h2>{error}</h2> : (
                <>
                    <div className='productscreen-left'>
                <div className='left-image'>
                    <img
                    src={product.imgUrl}
                    alt={product.name}
                    />
                </div>
                <div className='left-info'>
                    <p className='left-name'>{product.name}</p>
                    <p>Price: ${product.price}</p>
                    <p>Description: {product.description}</p>
                </div>
            </div>
            <div className='productscreen-right'>
                <div className='right-info'>
                    <p>
                        Price: <span>${product.price}</span>
                    </p>
                    <p>
                        Status: <span>
                        {product.countInStock > 0 ? "In stock" : "Out of stock"}
                        </span>
                    </p>
                    <p>
                        Quantity:
                        <select value={qty} onChange={(e) => {
                            setQty(e.target.value)
                            }}>
                            {[...Array(product.countInStock).keys()].map((x) => (
                                <option key={x+1} value={x+1}>{x+1}</option>
                            ))}
                        </select>
                    </p>
                    <p>
                        <button type="button" onClick={addToCartHandler}>Add To Cart</button>
                    </p>
                </div>
            </div>
                </>
            )}
        </div>
    )
};

export default ProductScreen;